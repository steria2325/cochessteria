package modelo.persistencia;

import java.util.List;

import modelo.entidades.Coche;

public interface DaoCoche {
	
	//no hace falta poner public, todos los metodos de la interfaz son p�blicos
		void insertar(Coche c);
		void modificar(Coche c);
		void borrar(Coche c);
		Coche buscar(int id);
		List<Coche> listar();
		List<Coche> ordenar(String tipo);
	
	
}
