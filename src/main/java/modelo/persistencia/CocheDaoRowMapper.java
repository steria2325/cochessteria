package modelo.persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import modelo.entidades.Coche;


@Component
public class CocheDaoRowMapper implements RowMapper<Coche>{
	
	@Override
	public Coche mapRow(ResultSet rs, int rowNum) throws SQLException {

		Coche c = new Coche();
		c.setId(rs.getInt("ID"));
		c.setMatricula(rs.getString("MATRICULA"));
		c.setModelo(rs.getString("MODELO"));
		c.setMarca(rs.getString("MARCA"));
		c.setKilometraje(rs.getDouble("KILOMETRAJE"));
		return c;
	}

}
