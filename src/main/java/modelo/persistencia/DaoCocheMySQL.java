package modelo.persistencia;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import modelo.entidades.Coche;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class DaoCocheMySQL implements DaoCoche{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private CocheDaoRowMapper rowMapper;
	

	@Override
	public void insertar(Coche c) {
		String query = "insert into coches (MATRICULA, MODELO, MARCA, KILOMETRAJE) values (?,?,?,?)";
		jdbcTemplate.update(query, c.getMatricula(),c.getModelo(),c.getMarca(),c.getKilometraje());
		
	}

	@Override
	public void modificar(Coche c) {
		String query = "update coches set MATRICULA=?, MODELO=?, MARCA=?, KILOMETRAJE=? where id=?";
		jdbcTemplate.update(query, c.getMatricula(),c.getModelo(),c.getMarca(),c.getKilometraje(), c.getId());
	}

	@Override
	public void borrar(Coche c) {
		String query = "delete from coches where id=?";
		jdbcTemplate.update(query, c.getId());
		
	}

	@Override
	public Coche buscar(int id) {
		String query = "select * from coches where id=?";
		//queryForObject espera solo un �nico objeto, con lo cual solo vale con b�squedas que buscan solo un objeto
		//jdbcTemplate.queryForObject llama internamente al m�todo mapRow() de la clase CocheDaoRowMapper, de la misma
		//manera que lo hace sort en un Comparator
		Coche c =  jdbcTemplate.queryForObject(query, rowMapper, id);
		return c;
		
	}

	@Override
	public List<Coche> listar() {
		String query="select * from coches";
		List<Coche> listaCoches = jdbcTemplate.query(query, rowMapper);
		return listaCoches;
	}

	@Override
	public List<Coche> ordenar(String tipo) {
		
		if(tipo.equals("marca")) {
			String query;
			 query="select * from coches order by marca";
			 List<Coche> listaCoches = jdbcTemplate.query(query, rowMapper);
				return listaCoches;
		}else {
			String query;
			 query="select * from coches order by modelo";
			 List<Coche> listaCoches = jdbcTemplate.query(query, rowMapper);
				return listaCoches;
		}
		
		
	}
	
}
