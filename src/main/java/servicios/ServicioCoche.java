package servicios;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import modelo.entidades.Coche;
import modelo.persistencia.DaoCoche;


@Service
public class ServicioCoche {
	
	
	
	@Autowired
	private DaoCoche daoCoche;

	public int insertar(Coche c) {
		if(7!=c.getMatricula().length()) {
			return 1;
		}else{
			if(this.daoCoche.listar().contains(c)) {
				return 2;
			}else {
				daoCoche.insertar(c);
				return 0;
			}
		}
	}
	
	
	
	public void modificar(Coche p) {
		daoCoche.modificar(p);
	}
	
	public void borrar(Coche p) {
		daoCoche.borrar(p);
	}
	
	public Coche buscar(int id) {
		return daoCoche.buscar(id);
	}	
	
	public List<Coche> listar() {
		return daoCoche.listar();
	}
	
	
	public List<Coche> ordenar(String tipo) {
		if(daoCoche.listar().isEmpty()) {
			return null;
		}else {
			return daoCoche.ordenar(tipo);
			
		}
		
	}


	
}
