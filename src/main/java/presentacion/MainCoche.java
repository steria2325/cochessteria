package presentacion;

import java.util.Comparator;
import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import config.Configuration;
import modelo.entidades.Coche;
import servicios.ServicioCoche;


public class MainCoche {

	public static ApplicationContext context;
	static {
		context = new AnnotationConfigApplicationContext(Configuration.class);
	}
	
	public static void main(String[] args) {
		ServicioCoche servicioCoche = context.getBean("servicioCoche", ServicioCoche.class);
		int opcion, resultado;
		String matricula, modelo, marca;
		double kilometraje;
		Comparator<Coche> comparador;
		do {
			System.out.println("Programa VendeCoches");
			System.out.println("====================");
			System.out.println("1) Alta Cochaso");
			System.out.println("2) Lista de Cochasos");
			System.out.println("3) Ordenar Cochasos por marca");
			System.out.println("4) Ordenar Cochasos por modelo");
			System.out.println("5) Buscar coche por ID");
			System.out.println("6) Borrar coche por ID");
			System.out.println("7) Salir del programa");
			System.out.println("====================");
			System.out.println();
			System.out.print("Introduzca una opcion: ");
			Scanner sc = new Scanner(System.in);
			opcion = Integer.parseInt(sc.nextLine());
			
			switch(opcion) {
			case 1:
				Coche c = context.getBean("coche", Coche.class);
				System.out.print("Inserta matricula: ");
				matricula = sc.nextLine();
				c.setMatricula(matricula);
				System.out.print("Inserta modelo: ");
				modelo = sc.nextLine();
				c.setModelo(modelo);
				System.out.print("Inserta marca: ");
				marca = sc.nextLine();
				c.setMarca(marca);
				System.out.print("Inserta kilometraje: ");
				kilometraje = Double.parseDouble(sc.nextLine());
				c.setKilometraje(kilometraje);
				
				resultado = servicioCoche.insertar(c);
				if (0==resultado) {
					System.out.println("Se ha a�adido satisfactoriamente");
				}else if(1==resultado) {
					  
						System.out.println("La matricula debe contener 7 caracteres");
					
				}else if(2==resultado){
					System.out.println("Esta matricula ya existe en la lista");
				}else {
					System.out.println("error de base de datos");
				}
				
				break;
			case 2:
				System.out.println(servicioCoche.listar());
				break;
			case 3:
				//comparador = context.getBean("servicioComparadorMarca", ServicioComparadorMarca.class);
				
				System.out.println(servicioCoche.ordenar("marca"));
				break;
			case 4:
				//comparador = context.getBean("servicioComparadorModelo", ServicioComparadorModelo.class);
				
				System.out.println(servicioCoche.ordenar("modelo"));
				break;
				
			case 5:
				
				System.out.print("Introduzca el ID del coche a buscar: ");
				String idBuscar=sc.nextLine();
				System.out.println(servicioCoche.buscar(Integer.parseInt(idBuscar)));
				
				break;
				
			case 6:
				
				System.out.print("Introduzca la ID del coche a borrar: ");
				Coche c2 = context.getBean("coche", Coche.class);
				String idBorrar=sc.nextLine();
				c2.setId(Integer.parseInt(idBorrar));
				servicioCoche.borrar(c2);
				break;
				
			case 7:
				System.out.println("ADIOS");
				return;
				
				default:
					break;
			}
			
		}while(opcion!=7);
		
		
		
	}

}
